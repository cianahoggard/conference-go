# Generated by Django 4.0.3 on 2022-12-30 11:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_conference_weather'),
    ]

    operations = [
        migrations.AddField(
            model_name='conference',
            name='coordinates',
            field=models.TextField(null=True),
        ),
    ]
